/*
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Прототипне наслідування це такий процес, при якому один об*єкт може наслідувати властивості іншого.Тобто у декількох
об*єктів можуть бути спільні властивості.
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
 Super використовується для того,щоб дочірній клас мав доступ до властивостей батька,без повторного їх запису.
*/

class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get _name(){
        return this.name;
    }
    set _name(value){
        this.name = value;
    }
    get _age(){
        return this.age;
    }
    set _age(value){
        this.age = value;
    }
    get _salary(){
        return this.salary;
    }
    set _salary(value){
        this.salary = value;
    }
}
class Programmer extends Employee{
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    get _salary(){
        return this.salary * 3 ;
    }
}

let programmer1 = new Programmer('Ira','22','6000','JS');
let programmer2 = new Programmer('Anna','42','6540','Python');
let programmer3 = new Programmer('Olga','33','7430','Java');
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

console.log(programmer1._salary,programmer2._salary,programmer3._salary);