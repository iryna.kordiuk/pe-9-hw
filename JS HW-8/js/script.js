/*
 1.Опишіть своїми словами що таке Document Object Model (DOM)
 DOM -це об'єктна модель документу, тобто це представлення html-документа у вигляді дома-дерева тегів, в якому теги є вузлами документу
 2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
  innerHtml повертає ввесь текст та вміст, включаючи внутрішні теги та інтервали елементів, а innerText повертає тільки
  текст без внутрішніх тегів та інтервалів елементів.
 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    До елемента сторінки можна звернутись за допомогою :

    1. ідентифікатора (ID): document.getElementById();
    2. класу (Class): document.getElementsByClassName();
    3. тегу або імені елемента: document.getElementsByTagName();
    4.селекторів: document.querySelector(), document.querySelectorAll()
        document.querySelector() - повертає перший елемент, що відповідає селектору
        document.querySelectorAll() - повертає колекцію всіх елементів, що відповідають селектору.
        document.getElementsByName() - використовуються для звернення до елементів за атрибутами

*/
//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
  const paragraph = document.querySelectorAll("p");
   paragraph.forEach(elem => elem.style.backgroundColor = "#ff0000");


    /*Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
    Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
    */
    const optionList = document.getElementById("optionsList");
    console.log(optionList);
    console.log(optionList.parentElement);// батьківський елемент.
     const childNodes = optionList.childNodes; // назви й тип дочірніх нодів.
    console.log(childNodes);
     optionList.childNodes.forEach(nodes =>{
         console.log("Назва :",nodes.nodeName);
         console.log("Тип:", nodes.nodeType);
         }
     )
    //Встановіть в якості контенту елемента з id testParagraph наступний параграф - This is a paragraph

    const testParagraph = document.getElementById("testParagraph");
    testParagraph.textContent = 'This is a paragraph';
    /*Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
    Кожному з елементів присвоїти новий клас nav-item.*/
    const mainHeader = Array.from(document.querySelector('.main-header').children);
    mainHeader.forEach((item) => {
    console.log(item);
    item.classList.add('nav-item');
});
//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
    const sectionTitle = document.querySelectorAll('.section-title');
    sectionTitle.forEach(item => item.classList.remove('section-title'));




