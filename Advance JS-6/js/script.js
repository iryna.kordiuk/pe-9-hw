const container = document.querySelector('.container');
const button = document.querySelector('.button');
 button.textContent = 'Get IP'

async function getIp(){
     const {data: {ip}} = await axios.get('https://api.ipify.org/?format=json');
     const {data:userInfo} = await axios.get(`http://ip-api.com/json/${ip}`);
    console.log(userInfo)
     return userInfo;
}
 button.addEventListener('click',async ()=>{
     container.innerHTML ='';
     const userData = await getIp();
     const {country:country,city:city,timezone:timezone,regionName:regionName,query:query} = userData;
     container.insertAdjacentHTML("beforeend",`
<p> Your IP : ${query}</p>
<p> Your continent : ${timezone.split('/')[0]}</p>
<p >Your country : ${country}</p>
<p> Your region : ${regionName}</p>
<p> Your city : ${city}</p>
`)
 })

