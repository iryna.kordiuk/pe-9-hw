/*
 1. Опишіть своїми словами як працює метод forEach.
  Метод forEach працює таким чином,що викликає один раз описану функцію для кожного елементу масиву,який ми передали
 2.Як очистити масив?
 Очистити масив ми можемо за допомогою методу arr.splice(). Синтаксис : array.splice(start[, deleteCount[, item1[, item2[, ...]]]]
 Щоб очистити масив повністю : array.splice(0,arr.length);
 3.Як можна перевірити, що та чи інша змінна є масивом?
 Для перевірки ми використовуємо метод Array.isArray(змінна). Якщо змінна є масивом, то повертає true;

* */


function filterBy(arr,typeOfParam){
 let newArr = arr.filter(elem => typeof(elem) != typeof (typeOfParam));
 return newArr ;
}


let array = ['hello', 'world', 23, '23', null];
console.log(array);// Масив до фільтрації
console.log(filterBy(array,'string')); //Масив після фільтрації
