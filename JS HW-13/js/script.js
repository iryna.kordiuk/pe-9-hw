/*
Теоретичні питання

1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
setTimeout дозволяє викликати функцію один раз з певною затримкою, а setInterval викликає цю функцію регулярно через заданий проміжок часу.

2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Заплановано буде максимально швидко, але планувальник викличе функцію тільки після виконання поточного коду. Бо планувальник перевіряє
календар тільки після виконання поточного коду.

3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Бо в пам'яті зберігається як сама функція setInterval, так і зовнішні змінні, на які вона використовує. Це зайве навантаження на пам'ять,
тому добре не забути викликати clearInterval, коли функція більше не потрібна.
*/

const imgList = document.querySelector('.images-wrapper');
    const images = document.querySelectorAll('.image-to-show');
    const startBtn =document.querySelector('.start-btn')
    const stopBtn = document.querySelector('.stop-btn');
    const firstImage = images[0];
    const lastImage = images[images.length - 1];


    const imageSlides = () => {

    const currentImage = document.querySelector('.visible');
    if(currentImage !== lastImage){
        currentImage.classList.remove('visible');
        currentImage.nextElementSibling.classList.add('visible');
    }else {
        currentImage.classList.remove('visible');
        firstImage.classList.add('visible');
    }
    }
    let timer = setInterval(imageSlides,5000);

    stopBtn.addEventListener('click', ()=>{
        clearInterval(timer);
        stopBtn.disabled = true;
        startBtn.disabled =false;
    })

    startBtn.addEventListener('click', ()=>{
        timer =setInterval(imageSlides,5000)
        startBtn.disabled = true;
        stopBtn.disabled =false;
    })


