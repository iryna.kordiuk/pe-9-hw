function saveThemePreference(theme) {
    localStorage.setItem('theme', theme);
}

function loadThemePreference() {
    return localStorage.getItem('theme');
}

function toggleTheme() {
    const body = document.body;
    if (body.classList.contains('light-theme')) {
        body.classList.remove('light-theme');
        body.classList.add('dark-theme');
        saveThemePreference('dark-theme');
    } else {
        body.classList.remove('dark-theme');
        body.classList.add('light-theme');
        saveThemePreference('light-theme');
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const themeButton = document.getElementById('themeButton');
    themeButton.addEventListener('click', toggleTheme);

    const savedTheme = loadThemePreference();
    if (savedTheme) {
        document.body.classList.add(savedTheme);
    } else {
        document.body.classList.add('light-theme');
    }
});
