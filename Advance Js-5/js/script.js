 class Card{
    constructor(name, email, title, text, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.id = id;

    }
    container = document.createElement('div');
    buttonContainer = document.createElement('div');
    deleteButton = document.createElement('button');

    deleteHandler(){
        this.buttonContainer.classList.add('button-container')
        this.deleteButton.textContent = 'Delete'
        this.deleteButton.addEventListener('click',()=>{
            axios.get(`https://ajax.test-danit.com/api/json/posts/${this.id}`).then(({status}) =>{
                    if(status === 200){
                        this.container.remove();
                    }
                }

            ).catch((err)=>{
                console.log(err)})
        })

    }

    render(){
        this.container.classList.add('container')
          this.container.insertAdjacentHTML('beforeend', `
              <div class="user-card">
              <div class="name">
             <span class = 'full-name'> ${this.name}</span>
             <span class="email">${this.email}</span>
            </div>
            
    <h2 class="title">${this.title}</h2>
    <p class="text">${this.text}</p>
    </div>
             

          `)
     document.body.append(this.container);
     this.buttonContainer.append(this.deleteButton);
     this.container.append(this.buttonContainer);
     this.deleteHandler();
    }
 }

 axios.get('https://ajax.test-danit.com/api/json/users')
     .then(({data}) => {
         const filteredArray = data.map(({id,name,email})=>{
            const newArr= { id,name,email};
            return newArr;

         })
         axios.get('https://ajax.test-danit.com/api/json/posts')
             .then(({data:post})=> {
                 filteredArray.forEach(({id,name,email})=>{
                     post.forEach(({id:postId,userId,title,body})=>{
                         if(userId === id){
                              new Card(name,email,title,body,postId).render()
                         }
                     })
                 })
             }).catch(err => {console.log( err)})
     }).catch(err => {throw new err})

