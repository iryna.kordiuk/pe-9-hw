import React from 'react';
import propTypes from "prop-types";
import './ModalHeader.sass'

function ModalHeader({children}) {
    return (
        <div className='modal-header'>{children}</div>
    );
}
ModalHeader.propTypes = {
    children: propTypes.node.isRequired
};

export default ModalHeader;