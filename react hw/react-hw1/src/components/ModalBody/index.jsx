import React from 'react';
import propTypes from "prop-types";
import './ModalBody.sass'
function ModalBody({children}) {
    return (
        <div className='body-container'>
            {children}

        </div>

    );
}
ModalBody.propTypes = {
    children : propTypes.node.isRequired
};

export default ModalBody;