import React from 'react';
import propTypes from "prop-types";
import Button from "../Buttons/Button.jsx";
import './ModalFooter.sass'
import '../Buttons/Buttons.sass'
function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick}) {
    return (
        <div className="modal__footer">
            {firstText && firstClick && (
                <Button classNames="modal__button" onClick={firstClick}>
                    {firstText}
                </Button>
            )}
            {secondaryText && secondaryClick && (
                <Button classNames="modal__button" onClick={secondaryClick}>
                    {secondaryText}
                </Button>
            )}
        </div>

    )
}
 ModalFooter.propTypes = {
     firstText : propTypes.string,
     secondaryText : propTypes.string,
     firstClick :propTypes.func,
     secondaryClick :propTypes.func,

 };


export default ModalFooter;