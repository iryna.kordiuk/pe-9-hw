import React from 'react';
import propTypes from "prop-types";
import './ModalImage.sass'

function ModalImage({src,alt}) {
    return (
        <>
            <img src={src} alt={ alt}/>
            <h1> Product Delete!</h1>
            <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
        </>




    )
}
ModalImage.propTypes = {
    src : propTypes.string.isRequired,
    alt : propTypes.string
};

export default ModalImage;