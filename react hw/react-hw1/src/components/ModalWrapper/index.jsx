import propTypes from "prop-types";
import './ModalWrapper.sass';
import React from 'react';

function ModalWrapper({onClick,children}) {

    return (
        <div onClick={onClick} className='modal-wrapper'>{children}</div>
    );
}
ModalWrapper.propTypes = {
    children: propTypes.node.isRequired,
};

export default ModalWrapper;