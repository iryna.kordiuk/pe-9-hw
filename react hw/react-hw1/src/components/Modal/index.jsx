import propTypes from "prop-types";
import './Modal.sass'
const Modal = ({onClick,children}) => {
    return(
        <div onClick={onClick} className='modal'>{children}</div>
    )
}

Modal.propTypes = {
    children: propTypes.node.isRequired
};

export default Modal;
