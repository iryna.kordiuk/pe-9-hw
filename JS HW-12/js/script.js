/*
Теоретичні питання

- Чому для роботи з input не рекомендується використовувати клавіатуру?
Для відслідковування дій користувача в input не рекомендується використовувати події клавіатури (keydown, keyup),
оскільки зміна даних в input може бути зроблена через вставку скопійованого тексту за допомогою мишки або засобами
розпізнаванням мовлення. Для відслідковування зміни даних в input ефективніше працюють події change, input, cut, copy, paste.
*/

let button = document.querySelectorAll('.btn');


document.documentElement.addEventListener('keydown',(event)=>{
    const keyName = event.code;

    button.forEach((elem)=>{

   let targetBtn = elem.innerHTML;
   let targetCode = `Key${targetBtn}`;
        if((keyName === 'Enter')&&(elem.innerHTML === keyName)||(keyName === targetCode)){
            elem.style.color ='blue';
        }else if(elem.style.color ==='blue'){
            elem.style.color = 'black'
        }
    })

})
