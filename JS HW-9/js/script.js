/* 1. Опишіть, як можна створити новий HTML тег на сторінці.
Створити новий html тег ми можемо за допомогою методу document.createElement("назва тегу").
   2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
    Перший параметр функції insertAdjacentHTML означає позицію елементу, який ми додаємо відносно елементу, який викликає метод.
    Може бути:
    -beforebegin - до самого елементу,що викликає функцію;
    -afterbegin - відразу після відкриваючого тега елементу;
    -beforeend - перед закриваючим тегом елемента;
    -afterend - після закриваючого  тега;
   3. Як можна видалити елемент зі сторінки?
   Для того ,щоб видалити елемент зі сторінки ми записуємо його в змінну та видаляємо за допомогою методу elem.remove().
   */
function createList(array, parent = document.body) {
    const arrayList = document.createElement("ul");
    parent.appendChild(arrayList);

    array.forEach((item) => {
        const listItem = document.createElement("li");
        arrayList.appendChild(listItem);

        if (Array.isArray(item)) {
            createList(item, listItem);
        } else {
            listItem.innerText = item;
        }
    });
}

createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);



