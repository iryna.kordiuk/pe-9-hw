/*
 Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
 . try...catch використовують для того, щоб замість падіння скрипта з виведенням помилки в консоль, виводилось щось більш логічне. Наприклад,
його часто використовують при роботі з JSON та JSON parse, щоб при наявності помилки в даних, скрипт не просто падав, а виводив логічне повідомлення
про помилку та вказував де конкретно виникла помилка.

 */
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let div = document.createElement('div');
    div.id = 'root';
 document.body.append(div);
 let list = document.createElement('ul');
    div.append(list);


for(let book of books){

     try{

         if((book.hasOwnProperty('author'))&&(book.hasOwnProperty('name'))&&(book.hasOwnProperty('price'))){
             let listItem = document.createElement('li');
             listItem.innerHTML = `Aвтор : "${book.author}"` + ` Назва: "${book.name}"` + ` Цiна: ${book.price}`;
             list.appendChild(listItem);

         }else if (!(book.hasOwnProperty('author'))){
             throw `в книзі "${book.name}" з ціною ${book.price}  не визначено автора.`;
         }else if (!(book.hasOwnProperty('name'))){
             throw  `в книзі  з ціною ${book.price} автора ${book.author}  не визначено назву.`;
         }else if (!(book.hasOwnProperty('price'))){
             throw `в книзі "${book.name}" автора ${book.author}  не визначено ціну.`;
         }
     }
     catch (e){
         console.log(e);
     }
}
