const burgerBtn = document.querySelector(".js-header__burger-button");
const closingBtn = document.querySelector(".js-header__closing-button")
const headerMenu = document.querySelector(".js-header__nav");
let lastActive;

burgerBtn.addEventListener('click',function (){
    headerMenu.classList.toggle('active');
    closingBtn.classList.toggle('active');

});
closingBtn.addEventListener('click',function (){
    headerMenu.classList.toggle('active');
    closingBtn.classList.toggle('active');
})


const itemTests = document.querySelectorAll(".header__nav-link");
itemTests.forEach(item => {
    item.addEventListener("click", function() {
        if (lastActive) {
            lastActive.classList.remove("header__nav-link-active");
        }
        this.classList.add("header__nav-link-active");
        lastActive = this;
    });
});




