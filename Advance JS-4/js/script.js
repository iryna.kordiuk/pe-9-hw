// AJAX - це така технологія, яка дозволяє нам динамічно працювати з сервером, запитами без повного перезавантаження сторінки,
// дає можливість створювати зручний WEB-інтерфейс і працювати з ним.
    const filmsList = document.createElement('ul');
    const actorsList = document.createElement('ul');
    document.body.append(filmsList);
    document.body.append(actorsList);

 fetch('https://ajax.test-danit.com/api/swapi/films').then(res => res.json()).then(
     film => {
         film.forEach(films =>{
             const listItem = document.createElement('li');
             listItem.innerHTML= `
             ${films.name}
             <ul class="film-characteristic">
            <li> Episode id : ${films.episodeId}</li>
            <li> Short info about film : ${films.openingCrawl}</li> 
            </ul>
             `
             filmsList.append(listItem)
             films.characters.forEach((url)=>{


                 fetch(url).then(res => res.json())
                     .then((data)=>{

                         const characterItem = document.createElement("li");
                         characterItem.innerHTML = `Actor: <a class="actor-link" href="${data.url}" target="_blank"> ${data.name} </a>`
                        listItem.insertAdjacentElement("beforeend", characterItem) ;
                     })

             })

         }
         )})
     .catch(error =>{
             console.log(` Помилка : ${error}`);
 })
