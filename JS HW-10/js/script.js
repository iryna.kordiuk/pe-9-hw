    const tabsBtn = document.querySelectorAll(".tabs-title");
    const tabsListItems = document.querySelectorAll(".tabs-list");
    tabsBtn.forEach(function (item){
        item.addEventListener("click",function (){
            let currentBtn = item;
            let tabId = currentBtn.getAttribute("data-tab");
            let currentTab = document.querySelector(tabId);
            tabsBtn.forEach(function (item){
                item.classList.remove('active');
            })
            tabsListItems.forEach(function (item){
                item.classList.remove('active');
            });
            currentBtn.classList.add('active');
            currentTab.classList.add('active');
        })
    })