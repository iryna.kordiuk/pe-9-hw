import React from 'react';
import propTypes from "prop-types";

function Button({type, classNames, onClick, children}) {
    return (
        <>
            <button type={type} className={classNames} onClick={onClick}>{children}</button>
        </>
    );
}
Button.propTypes = {
    type : propTypes.string,
    classNames: propTypes.string,
    onClick : propTypes.func,
    children : propTypes.node.isRequired,

};


export default Button;

