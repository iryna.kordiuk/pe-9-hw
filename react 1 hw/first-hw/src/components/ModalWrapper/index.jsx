 import propTypes from "prop-types";

 import React from 'react';

 function ModalWrapper({children}) {
     return (
         <div className='modal-wrapper'>{children}</div>
     );
 }
ModalWrapper.propTypes = {
    children: propTypes.node.isRequired,
};

 export default ModalWrapper;