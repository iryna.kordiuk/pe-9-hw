import React from 'react';
import propTypes from "prop-types";
import ModalHeader from "../ModalHeader/index.jsx";
function ModalImage({src,alt}) {
    return (
        <div>
            <img src={src} alt={ alt}/>

        </div>
    );
}
ModalHeader.propTypes = {
    src : propTypes.string.isRequired,
    alt : propTypes.string
};

export default ModalImage;