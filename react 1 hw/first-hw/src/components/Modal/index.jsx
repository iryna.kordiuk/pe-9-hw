 import propTypes from "prop-types";

const Modal = ({children}) => {
  return(
      <div className='modal'>{children}</div>
  )
}

 Modal.propTypes = {
     children: propTypes.node.isRequired
 };

 export default Modal;
