import React from 'react';
import propTypes from "prop-types";
function ModalBody({children}) {
    return (
        <div>{children}</div>
    );
}
ModalBody.propTypes = {
    children : propTypes.node.isRequired
};

export default ModalBody;