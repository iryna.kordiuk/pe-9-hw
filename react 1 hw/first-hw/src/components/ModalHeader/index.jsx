import React from 'react';
import propTypes from "prop-types";

function ModalHeader({children}) {
    return (
        <div className='modal-header'>{children}</div>
    );
}
ModalHeader.propTypes = {
    children: propTypes.node.isRequired
};

export default ModalHeader;